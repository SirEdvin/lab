import asyncio
from urllib.parse import urljoin

from sanic import Sanic, response
from sanic.request import Request

from anji_orm.model import Model
from anji_orm.fields import IntField, StringField
from anji_orm.core import register

from sanic_prometheus import monitor
from sanic_service_utils.common import (
    anji_orm_configuration, aiohttp_session_configuration,
    log_configuration, sentry_configuration
)

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Development"

app = Sanic('lab')
app.blueprint(anji_orm_configuration)
app.blueprint(aiohttp_session_configuration)
app.blueprint(log_configuration)
app.blueprint(sentry_configuration)

monitor(app).expose_endpoint()


class TestModel(Model):

    _table = 'test_model'

    counter = IntField(description='counter')
    name = StringField(secondary_index=True, definer=True)

    async def inc_and_save(self):
        self.counter += 1
        await self.async_send()


@app.listener('before_server_start')
async def configuration(sanic_app: Sanic, _loop: asyncio.AbstractEventLoop) -> None:
    sanic_app.config.setdefault('HTTP_CRON_ENDPOINT', 'http://127.0.0.1:8077')
    sanic_app.config.setdefault('SERVICE_ADDRESS', 'http://127.0.0.1:8977')


@app.route('/api/inc/<name>')
async def counter_inc(_request: Request, name: str) -> response.HTTPResponse:
    test_models = await (TestModel.name == name).async_run()
    if test_models:
        test_model = test_models[0]
    else:
        return response.json({'message': 'not found'}, status=404)
    await test_model.inc_and_save()
    return response.json({'message': 'ok'})


@app.route('/api/new/<name>')
async def counter_create(request: Request, name: str) -> response.HTTPResponse:
    test_models = await (TestModel.name == name).async_run()
    if test_models:
        return response.json({'message': 'already exists'}, status=403)
    else:
        test_model = TestModel(counter=0, name=name)
        asyncio.gather(
            test_model.async_send(),
            request.app.async_session.post(
                urljoin(request.app.config.HTTP_CRON_ENDPOINT, '/api/task'),
                json=dict(
                    url=urljoin(request.app.config.SERVICE_ADDRESS, f'/api/inc/{name}'),
                    name=f"Inc for {name}",
                    cron_expression="*/1 * * * *"
                )
            )
        )
    return response.json({'message': 'ok'})


@app.route('/')
async def counter_list(_request: Request) -> response.HTTPResponse:
    full_list = await register.async_execute(TestModel.all())
    return response.json([x.to_describe_dict() for x in full_list])


@app.route('/api/about')
async def about(_request: Request) -> response.HTTPResponse:
    return response.json({
        'name': 'PasteReport',
        'version': __version__,
        'license': __license__
    })
