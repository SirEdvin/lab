#!/usr/bin/env python3
import random
import click
import requests

from lab.app import app

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Development"


@click.group()
def cli():
    pass


@cli.command()
@click.option('--host', default='0.0.0.0')
@click.option('--port', default=8977, type=int)
@click.option('--debug', default=False, is_flag=True)
@click.pass_context
def run(ctx, host, port, debug):
    app.run(host=host, port=port, debug=debug)


if __name__ == "__main__":
    cli()
