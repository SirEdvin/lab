check:
	pylint lab
	pycodestyle lab
	mypy lab --ignore-missing-imports
