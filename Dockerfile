FROM alpine:3.7

# Install python

RUN apk add --no-cache python3 python3-dev gcc make musl-dev linux-headers \
    && mkdir -p /app/prometheus

COPY requirements.txt /requirements.txt

RUN python3 -m pip install -r /requirements.txt
WORKDIR /app

COPY "lab" "/app/lab"

EXPOSE 8977

ENV prometheus_multiproc_dir /app/prometheus
ENV GUNICORN_WORKERS_COUNT 2
ENV GUNICORN_BIND 127.0.0.1:8977

CMD ["/bin/sh", "-c", "/usr/bin/gunicorn -w ${GUNICORN_WORKERS_COUNT} -b ${GUNICORN_BIND} --capture-output --worker-class sanic.worker.GunicornWorker --log-level INFO lab.app:app"]
